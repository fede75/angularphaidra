
var app = angular.module("instantSearch", []);



app.filter('searchFor', function(){



	return function(arr, searchString){

		if(!searchString){
			return arr;
		}

		var result = [];

		searchString = searchString.toLowerCase();


		angular.forEach(arr, function(item){

			if(item.title.toLowerCase().indexOf(searchString) !== -1){
				result.push(item);
			}

		});

		return result;
	};

});



function InstantSearchController($scope){

	
	$scope.items = [
		{
			url: 'https://fc.cab.unipd.it/fedora/get/o:61439/bdef:Content/get',
			title: '61439',
			image: 'https://fc.cab.unipd.it/fedora/objects/o:61439/methods/bdef:Asset/getThumbnail'
		},
		{
			url: 'https://fc.cab.unipd.it/fedora/get/o:61435/bdef:Content/get',
			title: '61435',
			image: 'https://fc.cab.unipd.it/fedora/objects/o:61435/methods/bdef:Asset/getThumbnail'
		}
	];

}