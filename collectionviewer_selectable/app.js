// app.js
(function(){
define(function (require) {
  'use strict';

//  loadCss("//phaidrastaticdev.cab.unipd.it/collectionviewer/app/css/app.css");

//  function loadCss(url) {
//      var link = document.createElement("link");
//      link.type = "text/css";
//      link.rel = "stylesheet";
//      link.href = url;
//      document.getElementsByTagName("head")[0].appendChild(link);
//  } 

  require('angular');
  require('ngAnimate');
  require('ngTouch');
  require('ngSanitize');
  require('angularTranslate');
  require('angularTranslateLoaderStaticFiles');
  require('promiseTracker');
  require('dirPagination');

  // Set module and dependencies
  var app = angular.module('collectionApp', [
    'ngAnimate',
    'ngTouch',
    'ngSanitize',
    'angularUtils.directives.dirPagination',
    'pascalprecht.translate',
    'ajoslin.promise-tracker'
  ]);

  // Manually bootstrap app
  app.init = function () {
   //angular.element(document).find('head').prepend('<link type="text/css" rel="stylesheet" property="stylesheet" href="//phaidrastaticdev.cab.unipd.it/collectionviewer/app/css/app.css"/>');
    angular.bootstrap(document.getElementById('collectionApp'), ['collectionApp']);
  };
    
  // ----- APP CONFIG START -----
  app
    // Get template from url whitelisting        
    .config(function($sceDelegateProvider) {
      $sceDelegateProvider.resourceUrlWhitelist(['self', '**']);
    })

    // Localization config
    .config(['$translateProvider', function($translateProvider) {
//      $translateProvider.translations('it', {
//        'title'       : 'Titolo',
//        'rights'      : 'Diritti',
//        'coverage'    : 'Copertura',
//        'language'    : 'Language',
//        'creator'     : 'Autore',
//        'date'        : 'Data di caricamento',
//        'subject'     : 'Soggetto',
//        'description' : 'Description',
//        'identifier'  : 'URL',
//        'title  '     : 'Title',
//        'type'        : 'Tipologia',
//        'format'      : 'Formato',
//        'contributor' : 'Autore secondario',
//        'publisher'   : 'Editore',
//	      'source'      : 'Fonte',
//	      'relation'    : 'Relazione',
//        'All rights reserved': ' &copy; Tutti i diritti riservati',
//        'ita'         : 'Italiano',
//        'it'          : 'Italiano',
//	      'eng'	        : 'Inglese',
//        'en'          : 'Inglese',
//        'deu'         : 'Tedesco',
//        'lat'         : 'Latino',
//	      'fra'	        : 'Francese',
//	      'spa'	        : 'Spagnolo',
//        'ell'         : 'Greco moderno'
//      });

      // angular-translate sanitisation strategy
      // https://angular-translate.github.io/docs/#/guide/19_security
      // http://stackoverflow.com/questions/31002499/angular-translate-sanitisation-fails-with-utf-characters
      $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

      // Configure staticFilesLoader
      $translateProvider.useStaticFilesLoader({
        prefix: 'https://phaidrastaticdev.cab.unipd.it/collectionviewer/app/languages/',
        suffix: '.json'
      });

      // Load 'it' language on startup      	
      $translateProvider.preferredLanguage('it');
    }])

    .config(function($httpProvider) {
      // Allow multiple async call in same digest cycle
      $httpProvider.useApplyAsync(true);
    })

    //Disable debug info (production mode)
    //.config(['$compileProvider', function($compileProvider) {
    //  $compileProvider.debugInfoEnabled(false);
    //}])
    // ----- APP CONFIG END -----

    // Collection view directive
    .directive("collectionDirective", function() {
      return {
          templateUrl :"template.html"
      };
    })

    // Animation fading onload directive
    .directive('fadeIn', function(){
        return {
            restrict: 'AE',
            link: function($scope, $element, attrs) {
                $element.addClass("ng-hide-remove");                                 
                $element.on('load', function() {
                    $element.addClass("ng-hide-add"); 
                    $element.removeClass("loading"); 
                });
            }
        };
    })


    // Get Collection objects service
    .factory('collectionService', ['$http', function($http) {      
      var service = {
          getCollection: getCollection
      };
      return service;
      function getCollection(pid, from, limit, fields, instance) {
          return $http({
                          method  : 'GET',
                          url     : instance + '/object/' + pid + '/related',
                          params  : {
                                     'from': from,
                                     'limit': limit,  
                                     'relation': 'info:fedora/fedora-system:def/relations-external#hasCollectionMember',
                                     'fields': fields
                                    },
                          cache : true,
                          timeout: 60000
                       });
      }
    }])

    // Get metadata service
    .factory('metadataService', ['$http', function($http) {      
      var service = {
          getMetadata: getMetadata
      };
      return service;
      function getMetadata(pid, instance)    {
          return $http({
                       method : 'GET',
                       url    : instance + '/object/' + pid + '/dc',
                       cache  : true,
                       timeout: 60000
                    });
      }
    }])

    //Get MUSEO html 
    //Example: https://phaidradev.cab.unipd.it/render_metadata_museo/o:53203   
    .factory('metadataMuseoService', ['$http', function($http) {
      var service = {
          getMetadata: getMetadata
      };
      return service;
      function getMetadata(pid, instance)    {
          return $http({
                       method : 'GET',
                       url    : instance + '/render_metadata_museo/' + pid,
                       cache  : true,
                       timeout: 60000
                    });
      }
    }])

    // Search query in title, description (and PID) field filter 
    .filter('searchTitleDescFilter', function() {
      return function(items,query)  {
        if (query === '')  {
         return items;
        }
        else  {
          var filtered = [];
          var letterMatch = new RegExp(query, 'i');
          for (var i = 0; i < items.length; i++) {
              var item = items[i];
              if (letterMatch.test(item.title) || letterMatch.test(item.description) || letterMatch.test(item.pid)) {
                filtered.push(item);
              }
          }
          return filtered;
       }	
      };
    })

    // linkify url filter
    .filter('parseUrlFilter', function() {
      // Remove Phaidra 2.8 {link}{/link} tags
      var replacePattern = /({link}|{\/link})/gi;
      var urlPattern = /(http?|ftp):\/\/[\w-]+(\.[\w-]+)+([\w-.,@_?^=%&;:|!\/~+#()]*[\w-@_?^=%&;|!\/~+#()])?/gi;
      return function (text) {
        var text = String(text).trim(); 
        return (text.length > 0) ? text.replace(replacePattern, ' ').replace(urlPattern, '<a target="_blank" href="$&">$&</a>') : null;
      };
    })

    // html newline to <br /> filter
    .filter('nl2br', function() {
      return function(text){
        var text = String(text).trim(); 
        return (text.length > 0) ? text.replace(/[\r\n]/g, '<br />') : null;
      };
    })

    // highlight search query in text
    .filter('highlight', function() {
      return function(text, phrase) {
        var text = String(text).trim();
        return (phrase !== '') ? text.replace(new RegExp('('+phrase+')', 'gi'),
          '<span class="highlighted">$1</span>') : text;
      };
    })

    // COLLECTION CONTROLLER
    .controller('CollectionController', ['$scope', '$location', '$animate', '$translate', '$interval', 'promiseTracker', 'collectionService', 'metadataService', 'metadataMuseoService', function($scope, $location, $animate, $translate, $interval, promiseTracker, collectionService, metadataService, metadataMuseoService) {
  
//    $scope.phaidraInstance = 'https://phaidra.univie.ac.at';
//    $scope.phaidraInstance = 'https://phaidra.cab.unipd.it';
    $scope.phaidraInstance = 'https://phaidradev.cab.unipd.it';

//    $scope.apiInstance = 'https://services.phaidra.univie.ac.at/api';
//    $scope.apiInstance = 'https://phaidra.cab.unipd.it/api';
    $scope.apiInstance = 'https://phaidradev.cab.unipd.it/api';

//    $scope.fedoraInstance = 'https://fc.cab.unipd.it';
    $scope.fedoraInstance = 'https://fedoradev.cab.unipd.it';
  
    // Create a new promise tracker
    $scope.loadingCollectionTracker = promiseTracker();
    // Create a new promise tracker
//    $scope.loadingMetadataTracker = promiseTracker();
        
    // PID Error (from data-ng-init)
    $scope.pidError = 0;

    // Collection objects retrieve ('related' API call)
    $scope.objects = [];
    // Collection objects retrieve alerts
    $scope.collectionAlerts = [];
    // Collection objects retrieve error
    $scope.collectionError = {status: ''};

    // Collection metadata retrieve ('dc' API call)
    $scope.collectionMetadata = [];
    // Collection metadata retrieve alerts
    $scope.collectionMetadataAlerts = [];
    // Collection metadata retrieve error    
    $scope.collectionMetadataError = {status: ''};

    // Collection title
    $scope.collectionTitle = '';

    // Item metadata ('dc' API call)
    $scope.itemMetadata = [];
    // Item metadata and alerts
    $scope.itemMetadataAlerts = [];
    // Item metadata retrieve error
    $scope.itemMetadataError = {status:''};

    // Initializing search query to empty string
    $scope.query = '';

    //dirPagination-controls config
    $scope.maxSize = 7;
    $scope.autoHide = true;

    // Pagination start page
    $scope.currentPage = 1;

    // Show object metadata flag
    $scope.showMeta = 0;

    // Phaidra url get or view book
    $scope.resourceURL = '';

    // Hovering flag for horizontal collection navigation
    $scope.hovering = false;

    // Base url for permalink creation
    //$scope.baseUrl = $location.absUrl().split('#')[0].replace(/\/+$/, "");

    // Year in footer
    $scope.year = new Date().getFullYear();

    // App language
    $scope.appLanguage = 'ita';

    // Fallback app language
    $scope.appLanguageFallback = 'eng';

    // Maximum size of collection
    $scope.maxCollectionSize = 1000;

    // Set if size of collection higher than maxCollectionSize
    $scope.largeCollectionFlag = 0;

    // MUSEO datastream flag
    $scope.hasMUSEO = 0;
    // MUSEO metadata (preformatted html)
    $scope.MUSEOmetadata = '';

    // Get object detail function definition
    $scope.detail = detail;
	$scope.fetchCollection=fetchCollection;

    if ($scope.collection.pid !== "" && $scope.collection.pid.match(/^o\:[0-9]+$/)) {
      // Build numeric-only pid string for 'collection-container-XXXXX' class
      $scope.onlyPID = $scope.collection.pid.replace("o:","");

      // Get collection metadata
      getCollectionDetail($scope.collection.pid);

      // Fetch collection items
      fetchCollection();
    }
    else  {
      // Invalid PID
      $scope.pidError = 1;
    }

    // Horizontal navigation previous object
    $scope.prev = prev;
    function prev(posCurrent) {
      if ((posCurrent-1) >= 0) {
        $scope.detail($scope.objects[posCurrent-1]);
      }
    }

    // Horizontal navigation next object
    $scope.next = next;
    function next(posCurrent) {
      if ((posCurrent+1) <= ($scope.objects.length-1)) {
        $scope.detail($scope.objects[posCurrent+1]);
      }
    }

    // Process metadata for view
    function processMetadata(itemMetadata) {
      // Metadata fields
      var metadataObj = {
        'rights': {text:'', pos:8, sep:' - '},
        'coverage': {text:'', pos:5, sep:' - '},
        'language': {text:'', pos:7, sep:', '},
        'date': {text:'', pos:-1, sep:' - '},
        'subject': {text:'', pos:6, sep:' - '},
        'description': {text:'', pos:4, sep:' - '},
        'identifier': {text:'', pos:9, sep:' - '},
        'format': {text:'', pos:-1, sep:' - '},
        'type': {text:'', pos:-1, sep:' - '},
        'title': {text:'', pos:0, sep:' - '},
        'contributor': {text:'', pos:2, sep:' - '},
        'publisher': {text:'', pos:3, sep:' - '},
        'creator': {text:'', pos:1, sep:' - '},
        'source': {text:'', pos:-1, sep:' - '},
        'relation': {text:'', pos:-1, sep:' - '},
	'preview_page':{text:'', pos:-1, sep:' - '}
      };	
      	
      // Get metadata language ('language' field value)
      // TODO How to handle multiple object languages?
      $scope.metadataLanguage = itemMetadata.filter(function (el) {
        return el.xmlname === 'language';
      })[0].ui_value;
//    console.log($scope.metadataLanguage);

      var itemMetadataLength = itemMetadata.length;
      for (var i = 0; i < itemMetadataLength; i++) {
	var item = itemMetadata[i];
	// Remove not useful subject string from output 
	if (item.xmlname === 'subject') {
	  // Remove 'info:eu-repo...' subject string
	  if (item.ui_value.match(/^info\:eu-repo.*/i)) {	
	    continue;
	  }
	  if (item.attributes && item.attributes[0].xmlname === 'xml:lang') {
	    // Remove redundant 'Dewey...' subject string
	    if (item.ui_value.match(/^dewey.*/i) && item.attributes[0].ui_value === $scope.appLanguageFallback) {
	      continue;
	    }
	    // Remove redundant other languages subject string
	    if (item.attributes[0].ui_value !== $scope.appLanguage && item.attributes[0].ui_value !== $scope.appLanguageFallback) {
  	      continue;
	    }	  
	  }
	}
	// TODO handle multilanguage 'ita' and 'eng'
	if (metadataObj[item.xmlname].text) {
	  metadataObj[item.xmlname].text += metadataObj[item.xmlname].sep + $translate.instant(item.ui_value);
	}
	else {
	  metadataObj[item.xmlname].text += $translate.instant(item.ui_value);
	}
      }
      return metadataObj;
    }
  
    function processObjects(objects)  {
      
      objects = objects.sort(function(obj1, obj2) {
	if (obj1.pid < obj2.pid){
	  return -1;
	}else if (obj1.pid > obj2.pid){
	  return 1;
	}else{
	  return 0;
	}
      });
      angular.forEach(objects, function(item, index)  {
	// Set thumbnail URL                    
	item.thumbnailURL = $scope.phaidraInstance + '/preview/' + item.pid + '///120';
	// Set item position in collection
	item.pos = index;	     
      });
      return objects;
    }

    // Fetch collection objects
    function fetchCollection() {      
      var from = 1;
      var fields = ['dc.description'];
      var promise = collectionService.getCollection($scope.collection.pid, from, $scope.maxCollectionSize, fields, $scope.apiInstance);
      $scope.loadingCollectionTracker.addPromise(promise);
      promise.then(
        function(response)  {
	  //console.log(response);
	  if(response.data.alerts.length)  {
	    $scope.collectionAlerts = response.data.alerts;
	    console.log("WARNING: Alerts found in response fetching collection " + $scope.collection.pid);	    
	  }
	  else {
	    if (response.data.hits) {
	      $scope.hits = response.data.hits;
  	      if ($scope.hits > $scope.maxCollectionSize) {
	        $scope.largeCollectionFlag = 1;
	      }
	    }
	    if (typeof response.data.objects !== 'undefined' && response.data.objects.length)  {
	      $scope.objects = processObjects(response.data.objects);             
	      // Get metadata of first object in collection
	      //console.log($scope.objects[0]);
       	      $scope.detail($scope.objects[0]); 
	    }
	    else {
              $scope.collectionError.status = -1;
	      console.log("ERROR "  + $scope.collectionError.status + ": Get collection objects " + $scope.collection.pid + " error");
	    }
	  }
        },
        function(response) {
          if(response.data) {
	    $scope.collectionError.status = response.status;
            console.log("ERROR "  + $scope.collectionError.status + ": Get collection objects " + $scope.collection.pid + " failed");                      
          }
          if (response.status === -1) {
            $scope.collectionError.status = -1;
            console.log("ERROR " + $scope.collectionError.status + ": Get collection objects " + $scope.collection.pid + " connection timeouted");
          }
	}
      );        
      return;
    }

    // Get item metadata
    function detail(obj) {
      if (typeof obj === 'undefined') {
	      console.log("Current object undefined");
        return;
      }      
      // Check if object is already the current (avoid useless API calls)
      if ($scope.objCurrent === obj)  {
        return;
      }
      else  {
        $scope.objCurrent = obj;
      }
      // Set top-image URL (480px Phaidra image preview)
      $scope.topImageURL = $scope.phaidraInstance + '/preview/' + $scope.objCurrent.pid;
      // Get MUSEO html if it exists
      var promiseMuseo = metadataMuseoService.getMetadata($scope.objCurrent.pid, $scope.phaidraInstance);
      promiseMuseo.then(
	function(response) {
       	  if(typeof response.data !== 'undefined' && response.data.length){
            //console.log(response);
            $scope.MUSEOmetadata = response.data;
	    $scope.hasMUSEO = 1;
          }
        },
        function(response) {
	   //manage MUSEO metadata error
	   console.log('Can\'t found MUSEO metadata');
	}
      ).then(
	function(){
	  //console.log($scope.hasMUSEO);
          var promise = metadataService.getMetadata($scope.objCurrent.pid, $scope.apiInstance);      
          //$scope.loadingMetadataTracker.addPromise(promise);
          promise.then(
	    function(response) {
	    if(response.data.metadata.alerts.length) {
	      $scope.itemMetadataAlerts = response.data.metadata.alerts;
	      console.log("WARNING: Alerts found in metadata response for object " + $scope.objCurrent.pid)
	    }
	    else {
	      if (typeof response.data.metadata.dc !== 'undefined' && response.data.metadata.dc.length)  {
	        $scope.itemMetadata = response.data.metadata.dc;
	        $scope.metadataObj = processMetadata($scope.itemMetadata);
	        // Metadata field as array (full metadata view)
	        $scope.metadataArr = [];
	        angular.forEach($scope.metadataObj, function(val, key) {
		  $scope.metadataArr.push({label:key, text:val.text, pos:val.pos});
	        });
	        if ($scope.metadataObj.type.text)  {
	    	  switch($scope.metadataObj.type.text) {
  		    case 'Book':
	  	      $scope.resourceURL = $scope.fedoraInstance + '/fedora/get/' + $scope.objCurrent.pid + '/bdef:Book/view';
	 	        break;	
		      case 'Collection':
	  	        $scope.resourceURL = $scope.phaidraInstance + '/detail_object/' + $scope.objCurrent.pid;
	  	        break;
		      default:
	 	        $scope.resourceURL = $scope.fedoraInstance + '/fedora/get/' + $scope.objCurrent.pid + '/bdef:Content/get';
		  }
       	        }
	    }
	    else {
	      $scope.itemMetadataError.status = -1;
	      console.log("ERROR " + $scope.objCurrent.pid + ": metadata response empty");
	    }
	  }
	},
	function(response) {
	  if(response.data) {
	    $scope.itemMetadataError.status = response.status;
	    console.log("ERROR "  + $scope.itemMetadataError.status + ": Get object " + $scope.objCurrent.pid + " metadata retrieval failed");                        	      
	  }
	  if (response.status === -1) {
	    $scope.itemMetadataError.status = -1;
	    console.log("ERROR " + $scope.itemMetadataError.status + ": Get object " + $scope.objCurrent.pid + " metadata connection timeouted");	      	}
	}
      ); // Get DC metadata promise
      });
      return;
    }

    function getCollectionDetail(pid) {
      var promise = metadataService.getMetadata(pid, $scope.apiInstance);
//      $scope.loadingCollectionMetadataTracker.addPromise(promise);
      promise.then(
        function(response) {
          if(response.data.metadata.alerts.length) {
            $scope.collectionMetadataAlerts = response.data.metadata.alerts;
            console.log("WARNING: Alerts found in metadata response for object " + pid)
          }
          else {
	    if (typeof response.data.metadata.dc !== 'undefined' && response.data.metadata.dc.length) {
	      $scope.collectionMetadata = response.data.metadata.dc;
              $scope.collectionMetadataObj = processMetadata($scope.collectionMetadata);
	      // Title is the only field we are interested in collection object
	      if ($scope.collectionMetadataObj.title.text) {
  	        $scope.collectionTitle = $scope.collectionMetadataObj.title.text;
	      }
	      else {
		$scope.collectionMetadataError.status = -1;
		console.log("ERROR " + pid + ": metadata processing error");
	      }
	    }
	    else {
              $scope.collectionMetadataError.status = -1;
   	      console.log("ERROR " + pid + ": metadata response empty");
	    }
	  }	
        },
	function(response) {
          if(response.data) {
            $scope.collectionMetadataError.status = response.status;
            console.log("ERROR "  + $scope.itemMetadataError.status + ": Get object " + pid + " metadata retrieval failed");         
          }
          if (response.status === -1) {
            $scope.collectionMetadataError.status = -1;
            console.log("ERROR " + $scope.itemMetadataError.status + ": Get object " + pid + " metadata connection timeouted");      
          }
        }
      );
      return;
    }

    }]) // END CONTROLLER

  // Return app instance for requirejs
  return app;
});
})();