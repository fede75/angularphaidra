(function(){
require.config({
  paths: {
      'angular':'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min',
      'ngAnimate':'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-animate.min',
      'ngTouch':'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-touch.min',
      'ngSanitize':'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular-sanitize.min',
      'angularTranslate':'https://phaidrastaticdev.cab.unipd.it/collectionviewer/app/js/vendor/angular-translate.min',
      'angularTranslateLoaderStaticFiles':'https://phaidrastaticdev.cab.unipd.it/collectionviewer/app/js/vendor/angular-translate-loader-static-files.min',
      'promiseTracker':'https://phaidrastaticdev.cab.unipd.it/collectionviewer/app/js/vendor/promise-tracker.min',
      'dirPagination':'https://phaidrastaticdev.cab.unipd.it/collectionviewer/app/js/vendor/dirPagination.min'
  },
  baseUrl: "http://localhost/angularEVOLUTO",
  shim: {
      ngAnimate: {
          deps: ['angular'],
          exports: 'ngAnimate'
      },
      ngTouch: {
          deps: ['angular'],
          exports: 'ngTouch'
      },
      ngSanitize: {
          deps: ['angular'],
          exports: 'ngSanitize'
      },
      angularTranslate: {
          deps: ['angular'],
          exports: 'angularTranslate'
      },
      angularTranslateLoaderStaticFiles: {
          deps: ['angular', 'angularTranslate'],
          exports: 'angularTranslateLoaderStaticFiles'
      },
      promiseTracker: {
          deps: ['angular'],
          exports: 'promiseTracker'
      },
      dirPagination: {
          deps: ['angular'],
          exports: 'dirPagination'
      },
      angular: {
          exports: 'angular'
      }
  },
  waitSeconds: 0
});

require(['app'], function (app) {
  app.init();
});
})();