


var app = angular.module("app", []);

app.service("githubService", function($http, $q) {


  this.getAccount = function(idPhaidra) {
	  
	var deferred = $q.defer();

    return $http.get('https://phaidradev.cab.unipd.it/api/object/o:'+idPhaidra+'/dc')
      .then(function(response) {
       
        deferred.resolve(response.data);
        return deferred.promise;
      }, function(response) {
         
        deferred.reject(response);
        return deferred.promise;
      });
  };
});


app.controller("phaidradcpromise", function($scope, $q, githubService) {
/*
  githubService.getAccount('61439')
    .then(
      function(result) {
     
        $scope.dc = result;
      },
      function(error) {
        
        console.log(error.statusText);
      }
    );*/
	
	var promises=[];
	promises.push(githubService.getAccount('61439'));
    promises.push(githubService.getAccount('61435'));
	promises.push(githubService.getAccount('61433'));
	promises.push(githubService.getAccount('61432'));
	

	$q.all(promises).then(
        function(result) { 
            $scope.dc = result;
        }, 
        function(err) { 
            alert('err'); 
        }
    ).finally(function() {
        alert('fine');
    });
});