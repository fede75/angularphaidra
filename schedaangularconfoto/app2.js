
angular.module('objectListApp', [])
.factory('objectservice', function ($http) {
  
  var objects = []; 
  
    return {
      objects: objects,
      
      load: function (id) {
        return $http
        .get('http://phaidradev.cab.unipd.it/api/object/o:'+id+'/dc')
        .success(function (data) {
			objects.length=0;
           objects.push.apply(objects, data.metadata.dc);
        });
        
      },
      loadSingle: function (id, dc) {
        return $http.get('http://phaidradev.cab.unipd.it/api/object/o:'+id+'/dc' + dc);
        
      }
      
    };
})
.controller('objectsController', function ($scope, objectservice) {
  
  $scope.objects = objectservice.objects;
  
  $scope.objectID='';
  
  $scope.loadImage = function () {
	  $scope.objectID=$scope.inputOggetto;
	  objectservice.load($scope.objectID);

  };
  

  $scope.currentobject = null;
  
  $scope.showobject = function (dc) {
    if (dc) {
      objectservice.loadSingle($scope.objectID, dc).success(function (data) {
        $scope.currentobject = data;
      });
    } else {
      $scope.currentobject = null;
    }
    
  };
  
 
  
});
