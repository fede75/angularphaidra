﻿var myApp = angular.module('myApp', []);
myApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

myApp.filter('sumByKey', function () {
    return function (data, key) {
        if (typeof (data) === 'undefined' || typeof (key) === 'undefined') {
            return 0;
        }
        var sum = 0;
        for (var i = data.length - 1; i >= 0; i--) {
            sum += parseInt(data[i][key]);
        }
        return sum;
    };
})

myApp.controller('dc',
function ($scope, $http) {
 console.log('Entro nel controller');
 
 

 
 
 
   
		$http.get('https://phaidradev.cab.unipd.it/api/object/o:61439/dc')

        .success(function (data) {
			 console.log('SUCCESS: ' + JSON.stringify(data));

            $scope.listOfObjects = data.metadata.dc;

           
        })
        
        
});
